
# Directories
bin_DIR = bin

ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

# Files
bin = $(wildcard $(bin_DIR)/*)

.PHONY: default

# FYI
default:
	$(error This file is just used for installing/uninstalling the scripts for now!) 

install:
	install -d $(DESTDIR)/bin
	for b in $(bin); do \
		install -Dm755 $$b $(DESTDIR)/$(PREFIX)/$$b; \
	done

uninstall: 
	for b in $(bin); do \
		rm $(DESTDIR)/$(PREFIX)/$$b; \
	done
